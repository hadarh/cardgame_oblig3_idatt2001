package idatt2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.stage.Stage;

import java.io.File;


/**
 * JavaFX App
 */
public class App extends Application implements EventHandler<ActionEvent> {

    private Button dealHandButton;
    private Button showHandButton;
    private Label sumOutput;
    private Label heartsOutput;
    private Label flushOutput;
    private Label queenOutput;
    private ImageView image;
    private TilePane imageDisplayBox;
    private DealHand dealHand;
    private Image img;
    private int n = 12;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Card Game");

        dealHandButton = new Button("Deal Hand");
        dealHandButton.setPrefSize(80, 80);
        dealHandButton.setOnAction(this);

        showHandButton = new Button("Show Hand");
        showHandButton.setPrefSize(80, 80);
        showHandButton.setOnAction(this);

        HBox buttons = new HBox(40);
        buttons.getChildren().addAll(dealHandButton,showHandButton);

        Label sumLabel = new Label("Sum of the faces:");
        sumOutput = new Label ();
        sumOutput.setStyle("-fx-border-color: black; -fx-padding: 2px; -fx-min-width: 150px;");
        HBox hb1 = new HBox();
        hb1.getChildren().addAll(sumLabel, sumOutput);
        hb1.setSpacing(10);

        Label heartsLabel = new Label("Hearts:");
        heartsOutput = new Label ();
        heartsOutput.setStyle("-fx-border-color: black; -fx-padding: 2px; -fx-min-width: 150px;");
        HBox hb2 = new HBox();
        hb2.getChildren().addAll(heartsLabel, heartsOutput);
        hb2.setSpacing(10);

        Label flushLabel = new Label("Flush:");
        flushOutput = new Label ();
        flushOutput.setStyle("-fx-border-color: black; -fx-padding: 2px; -fx-min-width: 150px;");
        HBox hb3 = new HBox();
        hb3.getChildren().addAll(flushLabel, flushOutput);
        hb3.setSpacing(10);

        Label queenLabel = new Label("Queen of Spades:");
        queenOutput = new Label ();
        queenOutput.setStyle("-fx-border-color: black; -fx-padding: 2px; -fx-min-width: 150px;");
        HBox hb4 = new HBox();
        hb4.getChildren().addAll(queenLabel, queenOutput);
        hb4.setSpacing(10);

        VBox outputs = new VBox(20);
        outputs.getChildren().addAll(hb1,hb2,hb3,hb4);

        imageDisplayBox = new TilePane();
        imageDisplayBox.setMaxSize(680, 310);
        img = new Image("file:deck/deck.png");
        image =  new ImageView();
        image.setImage(img);
        imageDisplayBox.getChildren().add(image);
        image.setFitWidth(672);
        image.setFitHeight(300);
        imageDisplayBox.setStyle("-fx-border-color: black; -fx-padding: 2px;");

        AnchorPane layout = new AnchorPane();
        layout.getChildren().addAll(imageDisplayBox,buttons,outputs);

        layout.setTopAnchor(imageDisplayBox, 10.0);
        layout.setLeftAnchor(imageDisplayBox, 10.0);
        layout.setRightAnchor(imageDisplayBox,10.0);
        layout.setBottomAnchor(imageDisplayBox, 280.0);

        layout.setTopAnchor(buttons, 370.0);
        layout.setLeftAnchor(buttons, 100.0);
        layout.setRightAnchor(buttons,400.0);
        layout.setBottomAnchor(buttons, 10.0);

        layout.setTopAnchor(outputs, 370.0);
        layout.setLeftAnchor(outputs, 360.0);
        layout.setRightAnchor(outputs,10.0);
        layout.setBottomAnchor(outputs, 10.0);


        Scene scene = new Scene(layout, 700, 600);
        stage.setScene(scene);
        stage.show();


    }

    @Override
    public void handle(ActionEvent actionEvent) {

        if(actionEvent.getSource()==dealHandButton){
            imageDisplayBox.getChildren().clear();
            dealHand = new DealHand(n);
            image.setImage(new Image("file:deck/back.jpg"));
            imageDisplayBox.getChildren().add(image);
        }

        if (actionEvent.getSource()==showHandButton){
            imageDisplayBox.getChildren().clear();
            for(int i = 0; i < n; i++){
                ImageView tempImg = new ImageView(new Image("file:deck/" + dealHand.getRandomDealtDeck().get(i).getAsString() + ".jpg"));
                tempImg.setFitWidth(100);
                tempImg.setFitHeight(150);
                imageDisplayBox.getChildren().add(tempImg);
            }
            sumOutput.setText(String.valueOf(dealHand.sumOfFaces()));
            heartsOutput.setText(dealHand.displayHearts());
            flushOutput.setText(dealHand.checkFlush());
            queenOutput.setText(dealHand.checkQueen());
        }

    }
}