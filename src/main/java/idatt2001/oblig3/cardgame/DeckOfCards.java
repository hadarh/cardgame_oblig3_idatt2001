package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class DeckOfCards
 * @version 1.01 2021-04-06
 * @author Hadar Hayat
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deck;

    /**
     * Constructor for class DeckOfCards, no parameters, method to generate a full deck of 52 cards is integrated
     *
     */
    public DeckOfCards(){
        this.deck = new ArrayList<PlayingCard>(52);
        for (char s : suit){
            for (int i = 0; i <= 13; i++){
                deck.add(new PlayingCard(s, i));
            }
        }
    }

    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

}
