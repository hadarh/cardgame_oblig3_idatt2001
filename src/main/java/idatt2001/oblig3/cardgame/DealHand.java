package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class DealHand
 * @version 1.01 2021-04-06
 * @author Hadar Hayat
 */
public class DealHand {
    private DeckOfCards deckOfCards;
    private Random rand;
    private ArrayList<PlayingCard> randomDealtDeck;

    /**
     * Constructor for class DealHand, with all required parameters, method to pick an amount of random cards is integrated
     *
     * @param n
     */
    public DealHand(int n){
        this.deckOfCards = new DeckOfCards();
        this.rand = new Random();
        this.randomDealtDeck = new ArrayList<PlayingCard>();

        for (int y = 0; y < n; y++){
            int randomCard= rand.nextInt(deckOfCards.getDeck().size());
            randomDealtDeck.add(deckOfCards.getDeck().get(randomCard));

            //this is an optional line
            //only to ensure that we do not get the same card multiple times
            deckOfCards.getDeck().remove(randomCard);
        }
    }

    /**
     * @see getRandomDealtDeck
     *
     * @return  return list of random cards
     */
    public ArrayList<PlayingCard> getRandomDealtDeck(){
        return randomDealtDeck;
    }

    /**
     * @see sumOfFaces
     *
     * @return  return sum of all cards in random card deck
     */
    public int sumOfFaces(){return randomDealtDeck.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();}

    /**
     * @see sumOfFaces
     *
     * @return  return either 'No Hearts' or a string of all hearts present in the random card deck
     */
    public String displayHearts(){
        List<PlayingCard> tempList = randomDealtDeck.stream().filter(s->s.getAsString().startsWith("H")).collect(Collectors.toList());
        String result = "";
        if(tempList.size()==0){
            result = "No Hearts";
        }else{
            for(int a = 0; a<tempList.size(); a++){
                result += tempList.get(a).getAsString() + " ";
            }
        }
        return result;
    }

    /**
     * @see checkQueen
     *
     * @return  return either 'Yes' or 'No'
     */
    public String checkQueen(){
        String result = "No";
         if(randomDealtDeck.stream().anyMatch(a -> a.getAsString().equals("S12"))){
             result = "Yes";
         }
         return result;
    }

    /**
     * @see checkFlush
     *
     * @return  return either 'Yes' or 'No'
     */
    public String checkFlush(){
        String result = "No";
        if(randomDealtDeck.stream().filter(s->s.getAsString().startsWith("H")).collect(Collectors.toList()).size()>=5){
            result = "Yes";
        }else if(randomDealtDeck.stream().filter(s->s.getAsString().startsWith("C")).collect(Collectors.toList()).size()>=5){
            result = "Yes";
        }else if(randomDealtDeck.stream().filter(s->s.getAsString().startsWith("D")).collect(Collectors.toList()).size()>=5){
            result = "Yes";
        }else if(randomDealtDeck.stream().filter(s->s.getAsString().startsWith("S")).collect(Collectors.toList()).size()>=5){
            result = "Yes";
        }
        return result;
    }
}
